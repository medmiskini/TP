﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace OnlineShop.Models
{
    public class Order
    {
        public int OrderID { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
 
}