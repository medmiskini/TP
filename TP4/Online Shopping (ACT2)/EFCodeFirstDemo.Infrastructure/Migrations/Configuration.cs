namespace EFCodeFirstDemo.Infrastructure.Migrations
{
    using EFCodeFirstDemo.Core.Entities;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<EFCodeFirstDemo.Infrastructure.ShoppingContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(EFCodeFirstDemo.Infrastructure.ShoppingContext context)
        {
            context.Products.Add
            (
                  new Product
                  {
                      Product_Id = 1,
                      Name = "Ball",
                      Title = "Product One Ttile",
                      Description = "Product One Descriptopn",
                      Price = 11,
                      CategoryID = 1,
                      OrderID = 1
                  }
              );



            context.SaveChanges();

            base.Seed(context);
        }
    }
}
