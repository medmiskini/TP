﻿using EFCodeFirstDemo.Core.Entities;
using EFCodeFirstDemo.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCodeFirstDemo.Infrastructure
{
    public class ShoppingRepository : IProductRepository, ICategoryRepository, IOrderRepository
    {
        ShoppingContext context = new ShoppingContext();

        //Category
        public void AddCategory(Category category) {
            context.Categories.Add(category);
            context.SaveChanges();
        }

        public void EditCategory(Category category) {
            context.Entry(category).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }

        public void RemoveCategory(int Category_Id)
        {
            Category category = context.Categories.Find(Category_Id);
            context.Categories.Remove(category);
            context.SaveChanges();
        }

        public IEnumerable<Category> GetCategories()
        {
            return context.Categories;
        }

        public Category FindCategoryById(int Category_Id)
        {
            var c = (from r in context.Categories where r.Category_Id == Category_Id select r).FirstOrDefault();
            return c;
        }




        //Order
        public void AddOrder(Order order)
        {
            context.Orders.Add(order);
            context.SaveChanges();
        }

        public void EditOrder(Order order)
        {
            context.Entry(order).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }

        public void RemoveOrder(int Order_Id)
        {
            Order order = context.Orders.Find(Order_Id);
            context.Orders.Remove(order);
            context.SaveChanges();
        }

        public IEnumerable<Order> GetOrders()
        {
            return context.Orders;
        }

        public Order FindOrderById(int Order_Id)
        {
            var c = (from r in context.Orders where r.Order_Id == Order_Id select r).FirstOrDefault();
            return c;
        }




        //Product
        public void AddProduct(Product product)
        {
            context.Products.Add(product);
            context.SaveChanges();
        }

        public void EditProduct(Product product)
        {
            context.Entry(product).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }

        public void RemoveProduct(int Product_Id)
        {
            Product product = context.Products.Find(Product_Id);
            context.Products.Remove(product);
            context.SaveChanges();
        }

        public IEnumerable<Product> GetProducts()
        {
            return context.Products;
        }

        public Product FindProductById(int Product_Id)
        {
            var c = (from r in context.Products where r.Product_Id == Product_Id select r).FirstOrDefault();
            return c;
        }
    }
}
