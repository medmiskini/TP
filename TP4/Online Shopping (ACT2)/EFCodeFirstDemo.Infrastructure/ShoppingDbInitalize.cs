﻿using EFCodeFirstDemo.Core.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCodeFirstDemo.Infrastructure
{
    public class ShoppingDbInitalize : DropCreateDatabaseIfModelChanges<ShoppingContext>
    {
        protected override void Seed(ShoppingContext context)
        {
            //Adding initial Products data
            context.Products.Add
            (
                  new Product
                  {
                      Product_Id = 1,
                      Name = "Ball",
                      Title = "Product One Ttile",
                      Description = "Product One Descriptopn",
                      Price = 11,
                      CategoryID = 1,
                      OrderID = 1
                  }
              );

            

            context.SaveChanges();

            base.Seed(context);

        }
    }
}
