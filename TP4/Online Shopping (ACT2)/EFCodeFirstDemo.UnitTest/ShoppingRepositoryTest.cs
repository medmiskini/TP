﻿using System;
using System.Linq;
using EFCodeFirstDemo.Core.Entities;
using EFCodeFirstDemo.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EFCodeFirstDemo.UnitTest
{
    public class ShoppingRepositoryTest
    {
        ShoppingRepository Repo;

        [TestInitialize]
        public void TestSetup()
        {
            var db = new ShoppingContext();
            
            Repo = new ShoppingRepository();
        }

        #region Product  

        // check valid number of product/s(1) existing in current DB
        [TestMethod]
        public void IsRepositoryInitalizeWithValidNumberOfData_Product()
        {
            var result = Repo.GetProducts();
            Assert.IsNotNull(result);
            var numberOfRecords = result.ToList().Count;
            Assert.AreEqual(1, numberOfRecords);
        }

        // check add product method working and total number of product(2) correct
        [TestMethod]
        public void IsRepositoryAddsProducts()
        {
            Product productToInsert = new Product
            {
                Product_Id = 1,
                Name = "Ball",
                Title = "Product One Ttile",
                Description = "Product One Descriptopn",
                Price = 11,
                CategoryID = 1,
                OrderID = 1

            };
            Repo.AddProduct(productToInsert);
            // If Product inserts successfully, 
            //number of records will increase to 4 
            var result = Repo.GetProducts();
            var numberOfRecords = result.ToList().Count;
            Assert.AreEqual(2, numberOfRecords);
        }
        #endregion



    }

}
