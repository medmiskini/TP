﻿using EFCodeFirstDemo.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCodeFirstDemo.Core.Interfaces
{
    public interface IOrderRepository
    {
        void AddOrder(Order order);

        void EditOrder(Order order);

        void RemoveOrder(int Order_Id);

        IEnumerable<Order> GetOrders();

        Order FindOrderById(int Order_Id);
    }
}
