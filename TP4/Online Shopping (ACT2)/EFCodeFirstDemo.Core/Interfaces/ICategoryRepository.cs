﻿using EFCodeFirstDemo.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCodeFirstDemo.Core.Interfaces
{
    public interface ICategoryRepository
    {
        void AddCategory(Category category);

        void EditCategory(Category category);

        void RemoveCategory(int Category_Id);

        IEnumerable<Category> GetCategories();

        Category FindCategoryById(int Category_Id);
    }
}
