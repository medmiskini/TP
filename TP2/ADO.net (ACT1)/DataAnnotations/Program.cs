﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace DataAnnotations
{
    class Program
    {
        static void Main(string[] args)
        {
        }
    }
    public class User
    {
        public string Username { get; set; }
        public string DisplayName { get; set; }
    }
    public class Blog
    {
        public int BlogId { get; set; }
        [StringLength(1024)]
        public string Name { get; set; }
        public string Url { get; set; }
        public virtual List<Post> Posts { get; set; }
    }

    public class Post
    {
        [ScaffoldColumn(false)]
        public int PostId { get; set; }
        [DisplayName("PostTitle")]
        [Required(ErrorMessage = "A Post Title is required")]
        public string Title { get; set; }
        
        public string Content { get; set; }

        public int BlogId { get; set; }
        public virtual Blog Blog { get; set; }

        // Navigation property 
        public virtual ICollection<Post> AllPosts { get; private set; }
    }
    public class BloggingContext : DbContext
    {
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Post> Posts { get; set; }

        

            

    }
}
