﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace FluentAPI
{
    class Program
    {
        static void Main(string[] args)
        {
        }
    }
    public class User
    {
        public string Username { get; set; }
        public string DisplayName { get; set; }
    }
    public class Blog
    {
        public int BlogId { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public virtual List<Post> Posts { get; set; }
    }

    public class Post
    {
        public int PostId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }

        public int BlogId { get; set; }
        public virtual Blog Blog { get; set; }

        // Navigation property 
        public virtual ICollection<Post> AllPosts { get; private set; }
    }
    public class BloggingContext : DbContext
    {
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Post> Posts { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Model-Wide Settings
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            //HasKey
            modelBuilder.Entity<Post>()
            .HasKey(t => t.PostId);

            // IsRequired
            modelBuilder.Entity<Post>().Property(t => t.Title).IsRequired();
        }



    }
}
