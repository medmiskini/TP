﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
//If we tried to add a migration we’d get an error saying “EntityType ‘User’ has no key defined. Define the key for this EntityType.” 
//because EF has no way of knowing that Username should be the primary key for User.
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Core;
using System.Data.Entity.Core.Objects;

namespace GestionDesConcurrences
{

    class Program
    {
        static void Main(string[] args)
        {
            using (var db = new BloggingContext())
            {
                // Create and save a new Blog 
                Console.Write("Enter a name for a new Blog: ");
                var name = Console.ReadLine();

                var blog = new Blog { Name = name };
                db.Blogs.Add(blog);
                try
                {
                    // Try to save changes, which may cause a conflict.
                    int num = db.SaveChanges();
                    Console.WriteLine("No conflicts. " +
                        num.ToString() + " updates saved.");
                }
                catch (OptimisticConcurrencyException)
                {
                    // Resolve the concurrency conflict by refreshing the 
                    // object context before re-saving changes. 
                    db.Refresh(RefreshMode.ClientWins, blog);

                    // Save changes.
                    db.SaveChanges();
                    Console.WriteLine("OptimisticConcurrencyException "
                    + "handled and changes saved");
                }
            }
        }
    }

    public class Blog
    {
        public int BlogId { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public virtual List<Post> Posts { get; set; }
    }

    public class Post
    {
        public int PostId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }

        public int BlogId { get; set; }
        public virtual Blog Blog { get; set; }
    }
    public class User
    { //Data Annotation
        [Key]
        public string Username { get; set; }
        public string DisplayName { get; set; }
    }
    public class BloggingContext : DbContext
    {
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<User> Users { get; set; }

        //Fluent API
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .Property(u => u.DisplayName)
                .HasColumnName("display_name");
        }

        internal void Refresh(object clientWins, object orders)
        {
            throw new NotImplementedException();
        }
    }
}
