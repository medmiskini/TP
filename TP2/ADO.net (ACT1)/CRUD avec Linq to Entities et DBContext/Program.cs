﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
//If we tried to add a migration we’d get an error saying “EntityType ‘User’ has no key defined. Define the key for this EntityType.” 
//because EF has no way of knowing that Username should be the primary key for User.
using System.ComponentModel.DataAnnotations;

namespace CRUD_avec_Linq_to_Entities_et_DBContext
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var context = new BloggingContext())
            {   //Filtrage
                var Filterquery = context.Posts
                                   .Where(s => s.Title == "New")
                                   .FirstOrDefault<Post>();

                //GroupBy
                var Groupquery = from s in context.Posts
                            group s by s.Title into postsByTitle
                               select postsByTitle;

                foreach (var groupItem in Groupquery)
                {
                    Console.WriteLine(groupItem.Key);

                    foreach (var pst in groupItem)
                    {
                        Console.WriteLine(pst.PostId);
                    }

                }
            }
        }
        public class Blog
        {
            public int BlogId { get; set; }
            public string Name { get; set; }
            public string Url { get; set; }
            public virtual List<Post> Posts { get; set; }
        }

        public class Post
        {
            public int PostId { get; set; }
            public string Title { get; set; }
            public string Content { get; set; }

            public int BlogId { get; set; }
            public virtual Blog Blog { get; set; }
        }
        public class User
        { //Data Annotation
            [Key]
            public string Username { get; set; }
            public string DisplayName { get; set; }
        }
        public class BloggingContext : DbContext
        {
            public DbSet<Blog> Blogs { get; set; }
            public DbSet<Post> Posts { get; set; }
            public DbSet<User> Users { get; set; }

            //Fluent API
            protected override void OnModelCreating(DbModelBuilder modelBuilder)
            {
                modelBuilder.Entity<User>()
                    .Property(u => u.DisplayName)
                    .HasColumnName("display_name");
            }
        }
    }
}
