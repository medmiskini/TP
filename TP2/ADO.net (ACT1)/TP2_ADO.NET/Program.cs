﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2_ADO.NET
{
    class Program
    {
        

        static void Main(string[] args)
        {
            SqlConnection con = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=EtudiantDB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            string queryCreate =
                @"CREATE TABLE dbo.Products
                (
                 ID int IDENTITY(1,1) NOT NULL,
                 Name nvarchar(50) NULL,
                 Price nvarchar(50) NULL,
                 Date datetime NULL,
                    CONSTRAINT pk_id PRIMARY KEY (ID)
                );";

            string queryDropColumn =
                @"ALTER TABLE Etudiant
                  DROP COLUMN Description;";

            string queryAddColumn =
                @"ALTER TABLE Etudiant
                  ADD Description nvarchar(50);";
            string queryDropTable =
                @"DROP TABLE Etudiant";

            string queryAlterColumn = @"ALTER TABLE Etudiant
                  ALTER COLUMN Name int;";

            execute(queryDropColumn, con);
            
            
        }

        // CRUD

        static void execute(string query, SqlConnection con)
        {
            SqlCommand cmd = new SqlCommand(query, con);
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                Console.WriteLine("Table Modified Successfully");
            }
            catch (SqlException e)
            {
                Console.WriteLine("Error Generated. Details: " + e.ToString());
            }
            finally
            {
                con.Close();
                Console.ReadKey();
            }

        }

        // Filter data
        public void FilterData(SqlConnection con)
        {
            DataSet ds = new DataSet("Student");
            SqlDataAdapter da = new SqlDataAdapter("Select * from Student", con);
            da.TableMappings.Add("Table", "Student");
            da.Fill(ds);
            
        }



    }
}
