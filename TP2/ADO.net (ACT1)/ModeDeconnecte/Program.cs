﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModeDeconnecte
{
    class Program
    {
        static void Main(string[] args)
        {
            SqlConnection con = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=EtudiantDB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");

            string queryCreate =
                @"CREATE TABLE dbo.Products
                (
                 ID int IDENTITY(1,1) NOT NULL,
                 Name nvarchar(50) NULL,
                 Price nvarchar(50) NULL,
                 Date datetime NULL,
                    CONSTRAINT pk_id PRIMARY KEY (ID)
                );";


            string queryString = "SELECT ID, Name FROM dbo.Products";
            execute(queryCreate, con);
            SqlDataAdapter adapter = new SqlDataAdapter(queryString, con);

            DataSet products = new DataSet();
            adapter.Fill(products, "Customers");

            
            
        }
        static void execute(string query, SqlConnection con)
        {
            SqlCommand cmd = new SqlCommand(query, con);
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                Console.WriteLine("Table Modified Successfully");
            }
            catch (SqlException e)
            {
                Console.WriteLine("Error Generated. Details: " + e.ToString());
            }
            finally
            {
                con.Close();
                Console.ReadKey();
            }

        }
    }
}
