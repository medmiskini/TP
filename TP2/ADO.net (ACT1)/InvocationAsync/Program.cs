﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvocationAsync
{
    class Program
    {
        
            SqlCommand cmd = null;
            Boolean test = true;



            public static SqlConnection getConnection()
            {
                String strconn = @"Data Source = (localdb)\MSSQLLocalDB; Initial Catalog = EtudiantDB; Integrated Security = True; Connect Timeout = 30; Encrypt = False; TrustServerCertificate = True; ApplicationIntent = ReadWrite; MultiSubnetFailover = False";

                SqlConnection conn = new SqlConnection(strconn);
                return conn;
            }



            public void essai_Assync()
            {
                String strcmd = "SELECT * FROM Produit";
                SqlConnection conn = null;
                using (conn = getConnection())
                {
                    conn.Open();
                    cmd = new SqlCommand(strcmd, conn);
                    cmd.BeginExecuteReader(Essai_CallBack, null);
                    Console.WriteLine("waiting for response .....");
                    while (test)
                    {

                    }
                }

            }

            public void Essai_CallBack(IAsyncResult result)
            {


                IDataReader reader = cmd.EndExecuteReader(result);

                while (reader.Read())
                {
                    Console.WriteLine("nom: {0} | prenom: {1} | note: {2}", reader.GetString(1), reader.GetString(2), reader.GetInt32(3));
                }
                test = false;
            }
        

            static void Main(string[] args)
            {
            //***************************************** essai Assync
            Program eAo = new Program();
                eAo.essai_Assync();
            }

    
    }
}
