﻿using EFCodeFirstDemo.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCodeFirstDemo.Core.Interfaces
{
    public interface IProductRepository
    {
        void AddProduct(Product product);

        void EditProduct(Product product);

        void RemoveProduct(int Product_Id);

        IEnumerable<Product> GetProducts();

        Product FindProductById(int Product_Id);
    }
}
