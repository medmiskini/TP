﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCodeFirstDemo.Core.Entities
{
    public class Order
    {
        public int Order_Id { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}
