﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCodeFirstDemo.Core.Entities
{
    public class Product
    {
        [Key]
        public int Product_Id { get; set; }

        [ForeignKey("Category")]
        public int CategoryID { get; set; }

        
        public virtual Category Category { get; set; }

        [ForeignKey("Order")]
        public int OrderID { get; set; }

        
        public virtual Order Order { get; set; }

        public string Name { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public decimal Price { get; set; }
    }
}
