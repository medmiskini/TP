﻿using EFCodeFirstDemo.Core.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCodeFirstDemo.Infrastructure
{
    
    public class ShoppingContext : DbContext
    {
        public ShoppingContext() : base("name=shoppingappconnectionstring") // Connection String
        {

        }
        // Tables goin to create in Database
        public DbSet<Product> Products { get; set; }

        public DbSet<Category> Categories { get; set; }

        public DbSet<Order> Orders { get; set; }

    }
}
