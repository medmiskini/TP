﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    public class PrintNumber
    {
        public void Print()
        {
            for (long i = 1; i <= 100; i++)
            {
                Console.WriteLine(i);
            }
        }
    }
    class Threads
    {
        static void Main(string[] args)
        {
            PrintNumber printNumber = new PrintNumber();
            Thread t1 = new Thread(printNumber.Print);
            Thread t2 = new Thread(printNumber.Print);
            t1.Start();
            t2.Start();
            Console.ReadKey();
        }
    }
}
