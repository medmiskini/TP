﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Tasks
    {
        public static void Main()
        {
            Thread.CurrentThread.Name = "Main";

            // Creating a task 
            Task taskA = new Task(() => Console.WriteLine("Hello from taskA."));
            // Starting the task.
            taskA.Start();

            Console.WriteLine("Hello from thread '{0}'.",
                              Thread.CurrentThread.Name);
            taskA.Wait();
        }
    }
}
