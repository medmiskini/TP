﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Reflection_API
    {
        static void Main(string[] args)
        {
            int i = 42;
            System.Type type = i.GetType();
            System.Console.WriteLine(type);

            //OR

            System.Reflection.Assembly info = typeof(System.Int32).Assembly;
            System.Console.WriteLine(info); //getting information from assembly

        }
    }
}
