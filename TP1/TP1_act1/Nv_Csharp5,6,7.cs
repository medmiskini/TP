﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1TP1_act1
{
    class Nv_Csharp5_6_7
    {
        static void Async_prog()
        {
            public async Task Method1()
            {
                await Task.Run(() =>
                {
                    for (int i = 0; i < 100; i++)
                    {
                        Console.WriteLine(" Method 1");
                    }
                });
            }


            public void Method2()
            {
                for (int i = 0; i < 25; i++)
                {
                    Console.WriteLine(" Method 2");
                }
            }
        }
        static void string_interpolation()
        {
            string name = "Mark";
            var date = DateTime.Now;

            // Composite formatting:
            Console.WriteLine("Hello, {0}! Today is {1}, it's {2:HH:mm} now.", name, date.DayOfWeek, date);
            // String interpolation:
            Console.WriteLine($"Hello, {name}! Today is {date.DayOfWeek}, it's {date:HH:mm} now.");
           
        }
        //local functions:
        static void Main(string[] args)
        {

            int Sum(int x, int y)
            {
            return x + y;
            }

            Console.WriteLine(Sum(10, 20));

           

        }
    }
}
