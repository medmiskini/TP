﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1TP1_act1
{
    class etudiant
    {
        public string nom { get; set; }
        public int cin { get; set; }

        public override string ToString()
        {
            return(nom + " " + cin);
        }
    }
}
