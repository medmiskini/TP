﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1TP1_act1
{
    class Nv_Csharp3
    {
   

        delegate int cube(int i);//delegate for lambda expression
        static void Main(string[] args)
        {
   
            //impltypelocal();
            //anonymoustp();
            //linqexp();
            //ExtensionMethods();
            //objinit();
            //lamexp();

        }

        static void linqexp()
        {
            //linq simple example with array:
            int[] t = new int[5] { 1, 2, 3, 4, 5 };

            var query = from res in t  //query is an ienumerable<int> because it's selecting ints
                        where res > 2
                        select res;

            foreach (var re in query)
                Console.WriteLine(re);
        }

        static void ExtensionMethods()
        {
            int[] t = { 5, 2, 9, 3 };
            var res = t.OrderBy(x => x);
            foreach (var i in res)
            {
                Console.WriteLine(i);
            }

        }

        static void objinit()
        {
            etudiant e = new etudiant { nom = "mohamed", cin = 5 };
            Console.WriteLine(e.ToString());
        }

        static void anonymoustp()
        {
            var poly = new { a = 1, b = 5, c = 0 };//this an anonymous type
            var delta = poly.b * poly.b - 4 * poly.a * poly.c;
            Console.WriteLine("delta= " + delta);
        }

        static void impltypelocal()
        {
            //implicitly typed local variables:(at compile time)
            var k = 5; //k is an int
            var s = "something"; //s is a string
            var a = new[] { 12, 15 }; // a is int[]
        }

     
        static void lamexp()
        {
            //lambda expression:
            cube cubic = x => x * x * x;
            int n = cubic(3);
            Console.WriteLine(n);
        }



    }

}

