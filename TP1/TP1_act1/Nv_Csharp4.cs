﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1TP1_act1
{
    class Nv_Csharp4
    {
        static void dyvar()
        {
            //dynamic variables:(at runtime)
            dynamic y = "something";
            var len = y.Length;
            //var len2 = y.ddenk; //does not generate compile error
        }

        static void param_nommées()
        {
           
            PrintOrderDetails("Gift Shop", 31, "Red Mug");
            PrintOrderDetails(orderNum: 31, productName: "Red Mug", sellerName: "Gift Shop");
            PrintOrderDetails(productName: "Red Mug", sellerName: "Gift Shop", orderNum: 31);
            PrintOrderDetails("Gift Shop", 31, productName: "Red Mug");
            PrintOrderDetails(sellerName: "Gift Shop", 31, productName: "Red Mug");  
            PrintOrderDetails("Gift Shop", orderNum: 31, "Red Mug");
            void PrintOrderDetails(string sellerName, int orderNum, string productName)
            {
                if (string.IsNullOrWhiteSpace(sellerName))
                {
                    throw new ArgumentException(message: "Seller name cannot be null or empty.", paramName: nameof(sellerName));
                }

                Console.WriteLine($"Seller: {sellerName}, Order #: {orderNum}, Product: {productName}");
            }

        }

        static void param_optionnels()
        {
          
            ExampleClass anExample = new ExampleClass();
            anExample.ExampleMethod(1, "One", 1);
            anExample.ExampleMethod(2, "Two");
            anExample.ExampleMethod(3);
            ExampleClass anotherExample = new ExampleClass("Provided name");
            anotherExample.ExampleMethod(1, "One", 1);
            anotherExample.ExampleMethod(2, "Two");
            anotherExample.ExampleMethod(3);
            anExample.ExampleMethod(3, optionalint: 4);
        }



       
    }

    class ExampleClass
    {
        private string _name;
        public ExampleClass(string name = "Default name")
        {
            _name = name;
        }

       
        public void ExampleMethod(int required, string optionalstr = "default string",
            int optionalint = 10)
        {
            Console.WriteLine("{0}: {1}, {2}, and {3}.", _name, required, optionalstr,
                optionalint);
        }
    }
}

